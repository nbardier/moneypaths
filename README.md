# moneypaths

Search best routes for sending money, using quantum computing

The purpose of this code is to display a list of best possible ways to transfer money between two financial institutions, using a path search algorithm such as dijstra, and selecting a compute backend such as on premise or remote quantum computing
